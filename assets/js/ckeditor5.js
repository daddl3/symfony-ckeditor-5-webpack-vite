"use strict";

let defaultConfig;
let loadEditor;

import("../css/styles.css");

// Set of Initialized Textareas
const initializedTextareas = new Set();

let queue = Promise.resolve();

// Mutation Observer to Initialize Editors
const ckeditorObserver = new MutationObserver(async () => {
    if (ckeditorObserver.debounceTimeout) {
        clearTimeout(ckeditorObserver.debounceTimeout);
    }
    ckeditorObserver.debounceTimeout = setTimeout(async () => {
        let textareas = Array.from(
            document.querySelectorAll(
                "textarea[data-ckeditor5-config]:not(.ckeditor-initialized)"
            )
        );
        for (const textarea of textareas) {
            const textareaElement = textarea;

            // Check if textarea is not already initialized
            if (!initializedTextareas.has(textareaElement.id)) {
                initializedTextareas.add(textareaElement.id);

                // Add each initialization to the queue
                queue = queue.then(() => initializeEditor(textareaElement));
            }
        }
    }, 10);
});

// Object to Store Editors
const editors = {};

// Function to Initialize an Editor
async function initializeEditor(textarea) {
    const configModule = await import("./partials/defaultConfig.js");
    defaultConfig = configModule.defaultConfig;
    const editorLoaderModule = await import("./partials/editorLoader.js");
    loadEditor = editorLoaderModule.loadEditor;

    const configParse = JSON.parse(textarea.getAttribute("data-ckeditor5-config"));
    const config = configParse !== "default" ? configParse : defaultConfig;

    await processHtmlSupport(config);

    const darkMode = textarea.getAttribute("data-ckeditor5-darkmode");

    if (darkMode !== null && darkMode === "") {
        import("../css/prefer_color_scheme.css");
    }

    if (darkMode) {
        import("../css/darkmode.css");
    }

    let textareaName = textarea.name;
    const color = await getColorForTextarea(textareaName); // Get random or stored color for the textareaName
    await console.groupCollapsed(
        `%c[Editor: "${textareaName}"] Initialization Process`,
        `color: ${color}; font-weight: bold;`
    );

    try {
        const loadedEditor = await loadEditor(config, textareaName, color);

        const editorConfig = config.editor ?? "Classic";

        let htmlDOm = textarea;

        if (editorConfig !== "Classic" && editorConfig !== "Multi") {
            htmlDOm = await createEditorDiv(textarea, editorConfig, config);
        } else if (editorConfig === "Multi") {
            // Sicherstellen, dass md5 korrekt definiert ist und verwendet wird
            let hash = crypto.randomUUID();

            // Alle Elemente mit der Klasse aus der Config finden
            const rootElements = document.querySelectorAll(`.${config.multiEditorClass}`);

            const roots = {};

            // Jedes gefundene Element als Root definieren und eine passende ID verwenden
            await rootElements.forEach(async (element, index) => {
                const rootName = `${hash}${index + 1}`; // Benennung der Roots: editor1, editor2, etc.
                roots[rootName] = await createEditorDiv(element, editorConfig, config);
            });

            if (!textarea.classList.contains("ckeditor-initialized")) {
                // MultiRootEditor initialisieren
                const editor = await loadedEditor.create(roots, config);
                textarea.classList.add("ckeditor-initialized");

                await updateExtraTools(textarea, editorConfig, editor);

                // Daten-Synchronisation für alle Roots
                editor.model.document.on("change:data", () => {
                    Object.keys(roots).forEach((rootName) => {
                        const element = roots[rootName];

                        const textarea = document.querySelector(
                            `#${element.id.replace("_editor", "")}`
                        );

                        if (textarea) {
                            textarea.value = editor.getData({ rootName });
                        }
                    });
                });

                editors[textarea.id] = editor;

                await console.log(
                    `%c[MultiRootEditor: "${textarea.id}"] Successfully initialized.`,
                    `color: ${color};`
                );
            }
        }

        if (!textarea.classList.contains("ckeditor-initialized")) {
            const editor = await loadedEditor.create(htmlDOm, config);
            textarea.classList.add("ckeditor-initialized");

            await updateExtraTools(textarea, editorConfig, editor);

            editor.model.document.on("change:data", () => {
                textarea.value = editor.getData();
            });

            editors[textarea.id] = editor;

            await console.log(
                `%c[Editor: "${textarea.id}"] Successfully initialized.`,
                `color: ${color};`
            );
        }
    } catch (error) {
        await console.error(
            `%c[Error: "${textarea.id}"] Failed to initialize editor: ${error}`,
            `color: red; font-weight: bold;`
        );
    }

    await console.groupEnd(); // End main group
}

function isAdvancedRegex(str) {
    const regexSpecialChars = /[\^$\\.*+?()[\]{}|]/;

    const isAlreadyRegex = /^\/.*\/[gimsuy]*$/.test(str);

    if (regexSpecialChars.test(str) && !isAlreadyRegex) {
        try {
            new RegExp(str);
            return true;
        } catch (e) {
            return false;
        }
    }

    return false;
}

async function processHtmlSupport(config) {
    if (Array.isArray(config?.htmlSupport?.allow)) {
        await updateRegex(config.htmlSupport.allow);
    }

    if (Array.isArray(config?.htmlSupport?.disallow)) {
        await updateRegex(config.htmlSupport.disallow);
    }
}

const keysToCheck = ["name", "styles", "classes", "attributes"];
async function updateRegex(htmlSupportArray) {
    for (const item of htmlSupportArray) {
        for (const key of keysToCheck) {
            const value = item[key];
            // Check if value is not already a RegExp
            if (!(value instanceof RegExp)) {
                if (value && isAdvancedRegex(value)) {
                    item[key] = new RegExp(value);
                } else if (/^\/.*\/[gimsuy]*$/.test(value)) {
                    const parts = value.match(/^\/(.*)\/([gimsuy]*)$/);
                    item[key] = new RegExp(parts[1], parts[2]);
                }
            }
        }
    }
}

// Attach Editors to Window Object
window.editors = editors;

// Start Observer
ckeditorObserver.observe(document.body, {
    childList: true,
    subtree: true
});

// Map to store colors for each textareaName
const colorMap = {};

// Function to generate a random color
async function getRandomColor() {
    const letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

// Function to select the color for a textareaName, generates a new one if not already stored
async function getColorForTextarea(textareaName) {
    if (!colorMap[textareaName]) {
        // Generate a new color if none exists for this textareaName
        colorMap[textareaName] = await getRandomColor();
    }
    return colorMap[textareaName];
}

async function createEditorDiv(element, editorConfig, config) {
    if (element.tagName !== "TEXTAREA") {
        return element;
    }

    const div = document.createElement("div");
    div.id = element.id + "_editor"; // Eine neue ID für das div
    div.className = element.className; // Klassen von der textarea übernehmen
    div.innerHTML = element.value; // Inhalt der textarea ins div übertragen
    element.style.display = "none"; // Textarea ausblenden
    element.parentNode.insertBefore(div, element.nextSibling); // div nach der textarea einfügen

    let toolbarContainer = null;

    if (editorConfig === "Decouple" || editorConfig === "Multi") {
        toolbarContainer = document.createElement("div");
        toolbarContainer.id = element.id + "_toolbar"; // Eine eindeutige ID für den Toolbar-Container
        div.parentNode.insertBefore(toolbarContainer, div);
        if (config.menuBar?.isVisible) {
            toolbarContainer = document.createElement("div");
            toolbarContainer.id = element.id + "_menu-bar"; // Eine eindeutige ID für den Toolbar-Container
            div.parentNode.insertBefore(toolbarContainer, div);
        }
    }

    return div;
}

async function updateExtraTools(textarea, editorConfig, editor) {
    if (editorConfig === "Decouple" || editorConfig === "Multi") {
        const toolbarContainer = document.getElementById(textarea.id + "_toolbar");
        const menuBarContainer = document.getElementById(textarea.id + "_menu-bar");

        if (toolbarContainer) {
            await toolbarContainer.appendChild(editor.ui.view.toolbar.element);
        }

        if (menuBarContainer) {
            await menuBarContainer.appendChild(editor.ui.view.menuBarView.element);
        }
    }
}
