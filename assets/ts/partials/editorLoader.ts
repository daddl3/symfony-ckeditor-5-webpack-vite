let pushTranslation: any;
let pushTranslationPremium: any;
let loadPlugins: any;

const loadDependencies = async () => {
    const translationsModule = await import("./translations.js");
    pushTranslation = translationsModule.pushTranslation;
    pushTranslationPremium = translationsModule.pushTranslationPremium;

    const loadPluginsModule = await import("./loadPlugins.js");
    loadPlugins = loadPluginsModule.loadPlugins;
};

let ClassicEditor: object;

const loadEditor = async (config: any) => {
    if (ClassicEditor) {
        console.log("ClassicEditor already loaded");
        return ClassicEditor;
    }

    console.log("Loading ClassicEditor...");

    await loadDependencies();

    // Dynamischer Import des ClassicEditor Moduls
    const ClassicEditorModule = await import("@ckeditor/ckeditor5-editor-classic");
    ClassicEditor = ClassicEditorModule.ClassicEditor;

    config.translations = [];

    const language = config.language;

    if (typeof language === "string") {
        const trans = await pushTranslation(language);
        config.translations.push(trans.default);
    } else if (typeof language === "object") {
        if (language.ui) {
            const trans = await pushTranslation(language.ui);
            config.translations.push(trans.default);
        }
        if (language.content) {
            const trans = await pushTranslation(language.content);
            config.translations.push(trans.default);
        }
    }

    const Essentials = (await import("@ckeditor/ckeditor5-essentials/src/essentials.js"))
        .default;
    const Autoformat = (await import("@ckeditor/ckeditor5-autoformat/src/autoformat.js"))
        .default;
    const TextTransformation = (
        await import("@ckeditor/ckeditor5-typing/src/texttransformation.js")
    ).default;
    const Paragraph = (await import("@ckeditor/ckeditor5-paragraph/src/paragraph.js"))
        .default;
    const PasteFromOffice = (
        await import("@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice.js")
    ).default;
    const Markdown = (await import("@ckeditor/ckeditor5-markdown-gfm/src/markdown.js"))
        .default;
    const PasteFromMarkdownExperimental = (
        await import(
            "@ckeditor/ckeditor5-markdown-gfm/src/pastefrommarkdownexperimental.js"
        )
    ).default;
    const HtmlComment = (
        await import("@ckeditor/ckeditor5-html-support/src/htmlcomment.js")
    ).default;

    config.plugins = [
        Essentials,
        Autoformat,
        TextTransformation,
        Paragraph,
        PasteFromOffice,
        Markdown,
        PasteFromMarkdownExperimental,
        HtmlComment
    ];

    const toolbarItems = config.toolbar.items;
    await loadPlugins(toolbarItems, config.plugins);

    if (config.licenseKey) {
        if (typeof language === "string") {
            const trans = await pushTranslationPremium(language);
            config.translations.push(trans.default);
        } else if (typeof language === "object") {
            if (language.ui) {
                const trans = await pushTranslationPremium(language.ui);
                config.translations.push(trans.default);
            }
            if (language.content) {
                const trans = await pushTranslationPremium(language.content);
                config.translations.push(trans.default);
            }
        }

        const PasteFromOfficeEnhanced = (
            await import(
                "@ckeditor/ckeditor5-paste-from-office-enhanced/src/pastefromofficeenhanced.js"
            )
        ).default;
        config.plugins.push(PasteFromOfficeEnhanced);
    }

    config.translations = uniqueArray(config.translations);

    return ClassicEditor;
};

function uniqueArray(arr: any[]) {
    const uniqueSet = new Set(arr);
    return [...uniqueSet];
}

export { loadEditor };
