[Back to Usage](../usage.md)

### Enabling HTML Features

If you want to enable HTML features in your editor, you can add it as described in the official
documentation [link](https://ckeditor.com/docs/ckeditor5/latest/features/html/general-html-support.html):

```yaml
allow:
    - name: /.*/
```

or like this:

```yaml
allow:
    - name: .*
```

Another example:

```yaml
htmlSupport:
    {
        allow: [{ name: /.*/, attributes: true, classes: true, styles: true }],
        disallow:
            [{ name: /d(b+)d/g, attributes: true, classes: ["foo", "bar"], styles: true }]
    }
```

In the documentation, each element in `allow`/`disallow`, such as `name`, `attributes`, `classes`, and
`styles`, can have a pattern. To ensure these are converted properly, I check each string to see if it is a pattern and
then convert it into a regex. This does not apply if it is in an array or object.

<br>
<br>

[Back to Usage](../usage.md)
