[Back to Usage](../usage.md)

### Image Upload

#### Routes

Add a `routes.yaml` into your config and use the default controller:

```yaml
daddl3_symfonyckeditor5webpackvite_imageupload_upload:
    path: /ckeditor/upload
    controller: daddl3\SymfonyCKEditor5WebpackViteBundle\Controller\ImageUploadController::upload
    methods: [POST]

daddl3_symfonyckeditor5webpackvite_imageupload_browse:
    path: /ckeditor/browse
    controller: daddl3\SymfonyCKEditor5WebpackViteBundle\Controller\ImageUploadController::browse
    methods: [GET]
```

Otherwise, you need to configure the routes in your own controller or use `cloudServices`.

<br>
<br>

[Back to Usage](../usage.md)
