[Back to Usage](../usage.md)

### Vite Configuration

CKEditor5 is compatible with Vite without any additional configuration.
For projects using Vite, perform the following steps:

1. Import the CKEditor Vite plugin in your Vite configuration file:

```js
import ckeditor5 from "@ckeditor/vite-plugin-ckeditor5"; // docs says only need when using cdn
```

2. Add the CKEditor plugin to your Vite configuration's plugins array and the external option:

```js
export default {
    plugins: [ckeditor5({ theme: require.resolve("@ckeditor/ckeditor5-theme-lark") })]
    build: {
        rollupOptions: {
            external: id => /^@ckeditor\//.test(id), // This ensures that CKEditor plugins are loaded from the CDN or as separate chunks.
        }
    },
};
```

3. Specify the CKEditor script as an input in your Vite configuration:

```js
ckeditor5: "./vendor/daddl3/symfony-ckeditor-5-webpack/assets/js/ckeditor5.js";
```

<br>
<br>

[Back to Usage](../usage.md)
