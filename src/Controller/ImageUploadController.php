<?php

namespace daddl3\SymfonyCKEditor5WebpackViteBundle\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ImageUploadController extends AbstractController
{
    #[Route('/ckeditor/upload', name: 'daddl3_symfonyckeditor5webpackvite_imageupload_upload', methods: [Request::METHOD_POST])]
    public function upload(Request $request): JsonResponse
    {
        // Handle the file upload. Ensure you validate the file (e.g., for type and size)
        $uploadedFile = $request->files->get('upload'); // 'upload' is the default field name used by CKEditor for uploads

        if (!$uploadedFile) {
            return $this->json(['error' => ['message' => 'No file uploaded']], Response::HTTP_BAD_REQUEST);
        }

        // Perform file validation, e.g., file type and size

        /** @var string $projectDir */
        $projectDir = $this->getParameter('kernel.project_dir');

        // Save the file to a directory. You can use services like Symfony's FileSystem or Flysystem for this
        $destination = $projectDir.'/public/uploads'; // Example path, adjust as needed

        try {
            $uploadedFile->move($destination, $uploadedFile->getClientOriginalName());
        } catch (Exception $exception) {
            return $this->json(['error' => ['message' => 'Could not save file: '.$exception->getMessage()]], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        // Return a JSON response that includes the URL to the uploaded file. This URL is used by CKEditor to reference the image
        $url = $request->getSchemeAndHttpHost().'/uploads/'.$uploadedFile->getClientOriginalName();

        return $this->json(['url' => $url]);
    }

    #[Route('/ckeditor/browse', name: 'daddl3_symfonyckeditor5webpackvite_imageupload_browse', methods: [Request::METHOD_GET])]
    public function browse(): JsonResponse
    {
        /** @var string $projectDir */
        $projectDir = $this->getParameter('kernel.project_dir');

        // List files in the upload directory. This can be customized based on how you store and manage your uploads.
        $uploadDir = $projectDir.'/public/uploads'; // Example path, adjust as needed
        /** @var string[] $files */
        $files = scandir($uploadDir);

        $fileInfos = [];
        foreach ($files as $file) {
            if ('.' === $file || '..' === $file) {
                continue;
            }

            $filePath = $uploadDir.'/'.$file;

            /** @var string $appUrl */
            $appUrl = $this->getParameter('app.url');
            $url = $appUrl.'/uploads/'.$file; // Construct URL to access the file

            // Add file info to the response, customize this part as needed
            $fileInfos[] = [
                'name' => $file,
                'size' => filesize($filePath),
                'url' => $url,
            ];
        }

        // Return a JSON response with the list of files
        return $this->json($fileInfos);
    }
}
