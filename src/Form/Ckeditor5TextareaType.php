<?php

declare(strict_types=1);

namespace daddl3\SymfonyCKEditor5WebpackViteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Ckeditor5TextareaType extends AbstractType
{
    /**
     * @param array<string, mixed> $ckeditorConfigs
     */
    public function __construct(
        private readonly array $ckeditorConfigs,
    ) {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        // Normalizer to ensure the selected configuration exists or falls back to 'default'
        $resolver->setNormalizer('attr', function (OptionsResolver $resolver, $attr) {
            $configName = 'default';

            if (\array_key_exists('data-ckeditor5-config', $attr)) {
                $configName = $attr['data-ckeditor5-config'];
            }

            // Check if the provided config name exists in $this->ckeditorConfigs
            // If not, fall back to the 'default' configuration
            if (!\array_key_exists($configName, $this->ckeditorConfigs['editors'])) {
                $returnConfig = 'default';
            } else {
                $returnConfig = $this->ckeditorConfigs['editors'][$configName];
            }

            // Encode the returnConfig to JSON
            $encodedConfig = json_encode($returnConfig, \JSON_THROW_ON_ERROR);

            // Update the 'data-ckeditor5-config' key in $attr with the new encoded config
            $attr['data-ckeditor5-config'] = $encodedConfig;

            $darkMode = null;

            if (\array_key_exists('darkmode', $this->ckeditorConfigs)) {
                $darkMode = $this->ckeditorConfigs['darkmode'];
            }

            $attr['data-ckeditor5-darkmode'] = $darkMode;

            // Return the modified $attr array
            return $attr;
        });
    }

    public function getParent(): string
    {
        return TextareaType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'symfony_ckeditor5_textarea';
    }
}
